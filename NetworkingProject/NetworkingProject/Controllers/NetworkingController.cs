﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace NetworkingProject.Controllers
{
    public class NetworkingController : Controller
    {
        // GET: Networking
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult myAccount()
        {
            return View();
        }
        public ActionResult genealogy()
        {
            return View();
        }
        public ActionResult membership()
        {
            return View();
        }
        public ActionResult buy()
        {
            return View();
        }
        public ActionResult sell()
        {
            return View();
        }
        public ActionResult rewards()
        {
            return View();
        }
    }
}