﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(NetworkingProject.Startup))]
namespace NetworkingProject
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
